from django.contrib import admin
from .models import Hero, Homebase, Item

class HeroAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', 'gender', 'race', 'homebase', 'strength', 'vitality', 'intelligence', 'agility', 'dexterity', 'get_items')
    filter_horizontal = ('items',)

class HomebaseAdmin(admin.ModelAdmin):
    list_display = ('name', 'population', 'field_type')

class ItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'weight')

admin.site.register(Hero, HeroAdmin)
admin.site.register(Homebase, HomebaseAdmin)
admin.site.register(Item, ItemAdmin)