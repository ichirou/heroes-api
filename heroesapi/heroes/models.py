from django.db import models

class Homebase(models.Model):
    FIELDS = {
        ('TUN', 'Tundra'),
        ('MOU', 'Mountains'),
        ('PLN', 'Plains'),
        ('TRP', 'Tropics'),
        ('FOR', 'Forest'),
        ('JUN', 'Jungle'),
    }
    name = models.CharField(max_length=128)
    population = models.IntegerField(default=0)
    field_type = models.CharField(max_length=3, choices=FIELDS)

    def __str__(self):  # __unicode__ on Python 2
        return self.name

class Item(models.Model):
    TYPES = {
        ('WEP', 'Weapon'),
        ('ARM', 'Armor'),
        ('CON', 'Consumable'),
        ('ACC', 'Accessory'),
    }
    name = models.CharField(max_length=128)
    type = models.CharField(max_length=3, choices=TYPES)
    weight = models.IntegerField(default=1)

    def __str__(self):  # __unicode__ on Python 2
        return self.name

class Hero(models.Model):
    GENDER = {
        ('M', 'Male'),
        ('F', 'Female'),
    }
    RACES = {
        ('HUM', 'Human'),
        ('ELF', 'Elf'),
        ('DWR', 'Dwarf'),
        ('ORC', 'Orc'),
    }
    owner = models.ForeignKey('auth.User', related_name='heroes')
    name = models.CharField(max_length=128)
    gender = models.CharField(max_length=1, choices=GENDER)
    race = models.CharField(max_length=3, choices=RACES)
    homebase = models.ForeignKey(Homebase, on_delete=models.CASCADE, related_name='heroes')
    strength = models.IntegerField(default=0)
    vitality = models.IntegerField(default=0)
    intelligence = models.IntegerField(default=0)
    agility = models.IntegerField(default=0)
    dexterity = models.IntegerField(default=0)
    items = models.ManyToManyField(Item, related_name='owners')

    def get_items(self):
        return ", ".join([t.name for t in self.items.all()])

    get_items.short_description = "Items"

    class Meta:
        verbose_name_plural = "heroes"
        ordering = ('name', )

    def __str__(self):  # __unicode__ on Python 2
        return self.name

