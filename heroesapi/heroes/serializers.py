from django.contrib.auth.models import User
from rest_framework import serializers
from .models import Hero, Item, Homebase

# class UserSerializer(serializers.ModelSerializer):
#     heroes = serializers.PrimaryKeyRelatedField(many=True, queryset=Hero.objects.all())
#
#     class Meta:
#         model = User
#         fields = ('id', 'username', 'heroes')

# class HeroSerializer(serializers.ModelSerializer):
#    owner = serializers.ReadOnlyField(source='owner.username')
#     class Meta:
#         model = Hero
#         fields = ('owner', 'id', 'name', 'gender', 'race', 'homebase', 'strength', 'vitality', 'intelligence', 'agility', 'dexterity', 'items')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    heroes = serializers.HyperlinkedRelatedField(many=True, view_name='hero-detail', read_only=True)

    class Meta:
        model = User
        fields = ('url', 'pk', 'username', 'heroes')

class HeroSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    #items = serializers.HyperlinkedRelatedField(many=True, view_name='item-detail')

    class Meta:
        model = Hero
        fields = ('url', 'pk', 'owner', 'name', 'gender', 'race', 'homebase', 'strength', 'vitality', 'intelligence', 'agility', 'dexterity', 'items')

class HomebaseSerializer(serializers.HyperlinkedModelSerializer):
    heroes = serializers.HyperlinkedRelatedField(many=True, view_name='hero-detail', read_only=True)

    class Meta:
        model = Homebase
        fields = ('url', 'pk', 'name', 'population', 'field_type', 'heroes')

class ItemSerializer(serializers.HyperlinkedModelSerializer):
    owners = serializers.HyperlinkedRelatedField(many=True, view_name='hero-detail', read_only=True)

    class Meta:
        model = Item
        fields = ('url', 'pk', 'name', 'type', 'weight', 'owners')