from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^$', views.api_root),
    url(r'^users/$', views.UserList.as_view(), name='user-list'),
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='user-detail'),
    url(r'^heroes/$', views.HeroList.as_view(), name='hero-list'),
    url(r'^heroes/(?P<pk>[0-9]+)/$', views.HeroDetail.as_view(), name='hero-detail'),
    url(r'^homebases/$', views.HomebaseList.as_view(), name='homebase-list'),
    url(r'^homebases/(?P<pk>[0-9]+)/$', views.HomebaseDetail.as_view(), name='homebase-detail'),
    url(r'^items/$', views.ItemList.as_view(), name='item-list'),
    url(r'^items/(?P<pk>[0-9]+)/$', views.ItemDetail.as_view(), name='item-detail'),
]

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
]

urlpatterns = format_suffix_patterns(urlpatterns)