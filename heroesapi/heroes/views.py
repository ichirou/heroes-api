from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from rest_framework import status, generics, permissions
from .models import Hero, Item, Homebase
from .serializers import UserSerializer, HeroSerializer, ItemSerializer, HomebaseSerializer
from .permissions import IsOwnerOrReadOnly

@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'heroes': reverse('hero-list', request=request, format=format),
        'homebases': reverse('homebase-list', request=request, format=format),
        'items': reverse('item-list', request=request, format=format)
    })

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class HeroList(generics.ListCreateAPIView):
    queryset = Hero.objects.all()
    serializer_class = HeroSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class HomebaseList(generics.ListCreateAPIView):
    queryset = Homebase.objects.all()
    serializer_class = HomebaseSerializer

class ItemList(generics.ListCreateAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

class HeroDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Hero.objects.all()
    serializer_class = HeroSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly)

class HomebaseDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Homebase.objects.all()
    serializer_class = HomebaseSerializer

class ItemDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

# class HeroList(APIView):
#     """
#     List all heroes, or create a new hero.
#     """
#     def get(self, request, format=None):
#         heroes = Hero.objects.all()
#         serializer = HeroSerializer(heroes, many=True)
#         return Response(serializer.data)
#
#     def post(self, request, format=None):
#         serializer = HeroSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# class HomebaseList(APIView):
#     """
#     List all homebases, or create a new homebase.
#     """
#     def get(self, request, format=None):
#         homebases = Homebase.objects.all()
#         serializer = HomebaseSerializer(homebases, many=True)
#         return Response(serializer.data)
#
#     def post(self, request, format=None):
#         serializer = HomebaseSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# class ItemList(APIView):
#     """
#     List all items, or create a new item.
#     """
#     def get(self, request, format=None):
#         items = Item.objects.all()
#         serializer = ItemSerializer(items, many=True)
#         return Response(serializer.data)
#
#     def post(self, request, format=None):
#         serializer = ItemSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



# class HeroDetail(APIView):
#     """
#     Retrieve, update or delete a hero.
#     """
#     def get_object(self, pk):
#         try:
#             return Hero.objects.get(pk=pk)
#         except Hero.DoesNotExist:
#             raise Http404
#
#     def get(self, request, pk, format=None):
#         hero = self.get_object(pk)
#         serializer = HeroSerializer(hero)
#         return Response(serializer.data)
#
#     def put(self, request, pk, format=None):
#         hero = self.get_object(pk)
#         serializer = HeroSerializer(hero, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     def delete(self, request, pk, format=None):
#         hero = self.get_object(pk)
#         hero.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)

# class HomebaseDetail(APIView):
#     """
#     Retrieve, update or delete a homebase.
#     """
#     def get_object(self, pk):
#         try:
#             return Homebase.objects.get(pk=pk)
#         except Homebase.DoesNotExist:
#             raise Http404
#
#     def get(self, request, pk, format=None):
#         homebase = self.get_object(pk)
#         serializer = HomebaseSerializer(homebase)
#         return Response(serializer.data)
#
#     def put(self, request, pk, format=None):
#         homebase = self.get_object(pk)
#         serializer = HomebaseSerializer(homebase, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     def delete(self, request, pk, format=None):
#         hero = self.get_object(pk)
#         return Response(status=status.HTTP_204_NO_CONTENT)
#
# class ItemDetail(APIView):
#     """
#     Retrieve, update or delete an item.
#     """
#     def get_object(self, pk):
#         try:
#             return Item.objects.get(pk=pk)
#         except Item.DoesNotExist:
#             raise Http404
#
#     def get(self, request, pk, format=None):
#         item = self.get_object(pk)
#         serializer = ItemSerializer(item)
#         return Response(serializer.data)
#
#     def put(self, request, pk, format=None):
#         item = self.get_object(pk)
#         serializer = ItemSerializer(item, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     def delete(self, request, pk, format=None):
#         item = self.get_object(pk)
#         item.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)